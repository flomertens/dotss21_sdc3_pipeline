# DOTSS-21 SDC3 pipeline

This repository describe the methodoloy and code that the DOTSS-21 team used to take on the SDC3 challenges.

The general strategy followed by our team is based on the methodology developed for the LOFAR-EoR (e.g. [Mertens et al. 2020](https://ui.adsabs.harvard.edu/abs/2020MNRAS.493.1662M/abstract))and NenuFAR Cosmic Dawn ([Munshi et al. 2023](https://ui.adsabs.harvard.edu/abs/2023arXiv231105364M/abstract)) experiments adapated for this SKA simulated dataset.
- Compact sources are detected and subtracted from the raw visibilities using [OSKAR](https://github.com/OxfordSKA/OSKAR/).
- A model of the Galactic diffuse emission is build using [multi-scale cleaning](https://wsclean.readthedocs.io/en/latest/multiscale_cleaning.html), modeling the diffuse emission with a combination of Gaussian comonents. This diffuse emission is also subtracted from the raw visibilities using [OSKAR](https://github.com/OxfordSKA/OSKAR/).
- Seperation between the residual foregrounds and the 21-cm signal is performed with [ML-GPR](https://gitlab.com/flomertens/ps_eor) ([Mertens et al. 2018](https://ui.adsabs.harvard.edu/abs/2018MNRAS.478.3640M/abstract) and [Mertens et al. 2023](https://ui.adsabs.harvard.edu/abs/2023MNRAS.tmp.3280M/abstract)).
- Finally the power-spectra is computed with the [pspipe](https://gitlab.com/flomertens/pspipe) and [ps_eor](https://gitlab.com/flomertens/ps_eor) packages.

Our team submitted 3 results:
- An avoidance strategy in which ML-GPR is not performed and the power-spectra modes dominated by foregrounds are filtered.
- A full foregrounds removal strategy using ML-GPR on a image cube in which only the compact source has been subtracted.
- A full foregrounds removal strategy using ML-GPR on a image cube in which the compact source and diffuse emission model has been subtracted. This approach reduce the dynamic range of the residual foregrounds and make the task of ML-GPR easier, but with an additional risk.

For the general processing of the raw visibilities we used DP3. To predict the model visibilities we used OSKAR, including the beam. For the compact source model building we used PyBDSF. For imaging we used WSCLean.

# Version of software used:

- [ps_eor](https://gitlab.com/flomertens/ps_eor): version at commit 553f31404cf7332a1adb55ce89677bed528b41f5
- [pspipe](https://gitlab.com/flomertens/pspipe): version at commit 28cf18f8849779ec9776089191d80574c1917f20
- [DP3](https://github.com/lofar-astron/DP3/) version 5.3.0
- [WSClean](https://gitlab.com/aroffringa/wsclean) version 3.2
- [OSKAR](https://github.com/OxfordSKA/OSKAR) 2.8.3

# Sky model

All the sky models developed for this challenges can be found in the sky_models directory. Below we describe how they were obtained.

## Ateam and distant bright source model

The sky model ateam_ps_gleam2.osm is made of: 
- 6 Ateam sources: Tau A, Cygnus A, Cas A, For A, Pic A and Her A.
- 210 point-source from the Gleam catalog > 10 Jy @ 200 Mhz

## Bright in-field compact source model

These are the steps taken to build the bright in-field source model (bright.osm):

- Pre-process 91 channels (channels 0[0-9][0-9]0) with DP3 and the parset bright_model/dp3_average.parset
- Image the 91 channels with the parameters given in bright_model/img_config_sdc3.toml
- Use pybdsf to produce a sky model

## Faint in-field compact source model

These are the steps taken to build the faint in-field source model (faint_v1.osm):

- Pre-process 91 channels (channels 0[0-9][0-9]0) with DP3 (script faint_model/copy_data.sh)
- Predict model visibility for the bright source model with OSKAR (script faint_model/predict_bright.sh)
- Subtract the model visibily from the data (script faint_model/sub_bright_model.sh)
- Image the 91 channels with the parameters given in faint_model/img_config_sdc3.toml
- Use pybdsf to produce a sky model
- Post-process the sky model to discard sources with incorrect spectral index or very close to a bright source.

## Diffuse emission

To model the diffuse emission (diffuse_gaussian_and_point_components_trial7.osm) with a reduced risk to overfit the 21-cm signal, we used the following strategy:

- Pre-process 11 channels (channels 0[0-9]00) with DP3
- Image with wsclean with multi-scale option and take directly the sky model outputed by wsclean.
- Post-process the sky model to discard sources with incorrect spectral index

# Subtracting the sky models from the raw visibilities

To produce the sky model subtracted measurement sets, with sky model visibility predicted with OSKAR and subtracted from the raw visibilities, we used the following scripts:

- Script ps_imaging/batch_run_sub_combined.sh to create a MS dataset with the bright+faint model subtracted.
- Script ps_imaging/batch_run_sub_combined_diffuse.sh to create a MS dataset with the bright+faint+diffuse model subtracted.

# Residual foregrounds removal and Power-spectra

For the ML-GPR and power-spectra steps, we followed this steps:

- Create obs_ids corresponding to the different datasets (check [pspipe wiki](https://gitlab.com/flomertens/pspipe/-/wikis/home))
- Create images with pspipe (pspipe image run05.toml)
- Run ML-GPR for the different datasets, and for the two different cases (bright+faint model subtracted and bright+faint+diffuse model subtracted): use the batch_run.sh script for that.
- Produce the power-spectra for the 2 different case with the make_all_ml_gpr_res.py script.
- Plot and save the results with the notebook PS SDC3a-Public.ipynb

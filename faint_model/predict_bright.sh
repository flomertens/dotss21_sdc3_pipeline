#!/bin/bash

out_dir=/data/users/lofareor/mertens/sdc3/ms_u350-15000/c00-90/bright_model/
job_name=predict_bright.joblist

oskar_dir=/net/node100/data/users/lofareor/mertens/sdc3/oskar

run_oskar=${oskar_dir}/run_oskar.sh

sky_model=${oskar_dir}/bright.osm
template_setup_file=${oskar_dir}/setup_full.ini

parameters="--num_chan 1 --num_time_steps 360 --uv_min 350 --uv_max 15000 --int_time 40"

rm -f ${job_name}
mkdir -p logs

i=1

for c in $(seq -f "%03g" 00 90)
do
  n=$(printf "%02d" $i)
  mkdir -p /net/node1${n}/${out_dir}

  msout="${out_dir}/ZW3_IFRQ_${c}0.ms"

  setup_full="${out_dir}/ZW3_IFRQ_${c}0.ms.setup_full.ini"
  cp ${template_setup_file} /net/node1${n}/${setup_full}

  freq=$(echo "scale=2; 106 + $c" |bc)

  cmd="${run_oskar} ${setup_full} ${msout} ${sky_model} ${parameters} --start_freq ${freq}"
  echo $cmd

  nodetool add_job ${job_name} "${cmd}" --log_file logs/ZW3_IFRQ_${c}0.ms.model_ateam.log -t node1${n}

  i=$(( (i % 16) + 1 ))
done

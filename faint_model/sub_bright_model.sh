#!/bin/bash

in1_dir=/data/users/lofareor/mertens/sdc3/ms_u350-15000/c00-90/data/
in2_dir=/data/users/lofareor/mertens/sdc3/ms_u350-15000/c00-90/bright_model/
out_dir=/data/users/lofareor/mertens/sdc3/ms_u350-15000/c00-90/data_sub_bright/
name=sub_bright_model

job_name=${name}.joblist

rm -f ${job_name}
mkdir -p logs

sub_model=/net/node100/data/users/lofareor/mertens/sdc3/sub_model.py

i=1

for c in $(seq -f "%03g" 00 90)
do
  n=$(printf "%02d" $i)
  mkdir -p /net/node1${n}/${out_dir}

  msout="${out_dir}/ZW3_IFRQ_${c}0.ms"
  msin1="${in1_dir}/ZW3_IFRQ_${c}0.ms"
  msin2="${in2_dir}/ZW3_IFRQ_${c}0.ms"

  cmd="${sub_model} ${msin1} ${msin2} ${msout}"
  echo $cmd

  nodetool add_job ${job_name} "${cmd}" --log_file logs/ZW3_IFRQ_${c}0.ms.${name}.log -t node1${n}

  i=$(( (i % 16) + 1 ))
done

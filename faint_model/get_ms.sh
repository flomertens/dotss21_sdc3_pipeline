#!/bin/bash

out_dir=/data/users/lofareor/mertens/sdc3/ms_u350-15000/c00-90/data_sub_bright/

i=1

for c in $(seq -f "%03g" 00 90)
do
  n=$(printf "%02d" $i)
  echo -n " /net/node1${n}/${out_dir}/ZW3_IFRQ_${c}0.ms"
  i=$(( (i % 16) + 1 ))
done


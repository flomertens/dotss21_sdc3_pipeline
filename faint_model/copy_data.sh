#!/bin/bash

sdc_data_dir=/net/node100//data/users/lofareor/offringa/SKA-SDC3a-bugfixed/MS/
out_dir=/data/users/lofareor/mertens/sdc3/ms_u350-15000/c00-90/data/
job_name=copy_data.joblist

rm -f ${job_name}
mkdir -p logs

i=1

for c in $(seq -f "%03g" 00 90)
do
  n=$(printf "%02d" $i)
  mkdir -p /net/node1${n}/${out_dir}
  cmd="DP3 dp3_copy.parset msin=${sdc_data_dir}/ZW3_IFRQ_${c}0.ms msout=/net/node1${n}/${out_dir}/ZW3_IFRQ_${c}0.ms"
  echo $cmd
  nodetool add_job ${job_name} "${cmd}" --log_file logs/ZW3_IFRQ_${c}0.ms.log
  i=$(( (i % 16) + 1 ))
done


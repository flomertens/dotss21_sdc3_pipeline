#!/bin/bash

out_dir=/data/users/lofareor/mertens/sdc3/ms_u350-15000/c00-90/bright_model/
job_name=filter_uvrange.joblist

rm -f ${job_name}
mkdir -p logs

dp3_param="msout.overwrite=true steps=[filter] filter.blrange=[350,15000]"

i=1

for c in $(seq -f "%03g" 00 90)
do
  n=$(printf "%02d" $i)
  mkdir -p /net/node1${n}/${out_dir}

  msin="${out_dir}/ZW3_IFRQ_${c}0.ms"

  cmd="DP3 msin=${msin} msout=${msin}.uvcut ${dp3_param} && rm -rf ${msin}.old && mv ${msin} ${msin}.old && mv ${msin}.uvcut ${msin} && rm -r ${msin}.old"
  #cmd="mv ${msin}.old ${msin}"

  echo $cmd

  nodetool add_job ${job_name} "${cmd}" --log_file logs/ZW3_IFRQ_${c}0.ms.filter_uvrange.log -t node1${n}

  i=$(( (i % 16) + 1 ))
done

#!/bin/bash

oskar_dir=/net/node100/data/users/lofareor/mertens/sdc3/oskar/
oskar_sif=${oskar_dir}/oskarpipe.sif
run_oskar=${oskar_dir}/run_oskar.py

binds=$(for node in {100..116}; do echo "-B /net/node${node}"; done)

singularity exec ${binds} -B /data ${oskar_sif} ${run_oskar} $@

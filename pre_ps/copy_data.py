#!/usr/bin/env python3
import os
import toml
import click
import itertools

from libpipe import worker


def get_channels(channel_str):
    return sorted(set(channel for k in channel_str.split(',') for channel in worker.expend_num_ranges(k)))


@click.command()
@click.argument('config-file', type=click.Path(exists=True))
@click.argument('name', type=str)
@click.option('--max_concurrent', '-m', help='Maximum concurrent tasks on a node', type=int, default=2)
@click.option('--env_file', '-e', help='Environment source file', type=click.Path(exists=True))
@click.option('--dry_run', '-d', help='Dry run, only print commands', is_flag=True)
@click.option('--force', is_flag=True, help='Force execution without confirmation')
@click.option('--only_channels', help='Process only specific channels', default=None)
def main(config_file, name, max_concurrent, env_file, dry_run, force, only_channels):
    ''' Copy MS from the main directory, distributing them to different nodes using the CONFIG_FILE'''
    # Load configuration from TOML file
    config = toml.load(config_file)
    sdc_data_dir = config['sdc_data_dir']
    base_dir = config['base_dir']
    uv_min = config['uv_min']
    uv_max = config['uv_max']
    input_time_steps = config['input_time_steps']
    avg_time_step = config['avg_time_step']

    if only_channels:
        only_channels = get_channels(only_channels)

    nodes = [*worker.expend_num_ranges(config['nodes'])]
    nodes_cycle = itertools.cycle(nodes)
    channels = worker.expend_num_ranges(config['channels'])

    dp3_params = f'numthreads=24 msout.overwrite=true msin.ntimes={input_time_steps} steps=[filter,averager] filter.blrange=[{uv_min},{uv_max}] averager.timestep={avg_time_step}'

    os.makedirs('logs', exist_ok=True)

    worker_pool = worker.WorkerPool([worker.localhost_shortname], name='Copy Data', max_tasks_per_worker=max_concurrent,
                                    env_source_file=env_file, debug=dry_run, dry_run=dry_run)

    for node, channel in zip(nodes_cycle, channels):
        if only_channels and channel not in only_channels:
            continue

        output_dir = f'/net/{node}/{base_dir}/{name}'

        os.makedirs(output_dir, exist_ok=True)

        cmd = f'ionice -c 3 DP3 {dp3_params} msin={sdc_data_dir}/ZW3_IFRQ_{channel}.ms msout={output_dir}/ZW3_IFRQ_{channel}.ms'

        log_file = f'logs/ZW3_IFRQ_{channel}.ms.log'
        print(cmd)
        worker_pool.add(cmd, output_file=log_file)

    confirm_execute = force or click.confirm('Do you want to execute the tasks?')

    if confirm_execute:
        worker_pool.execute()

if __name__ == '__main__':
    main()

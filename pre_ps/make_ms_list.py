#!/usr/bin/env python3
import os
import toml
import click
import itertools

from libpipe import worker

out_dir = 'ps/ms_lists'

def chanel_to_freq(channel):
    return 106 + int(channel) / 10

@click.command()
@click.argument('config-files', type=click.Path(exists=True), nargs=-1)
@click.argument('name', type=str, nargs=1)
@click.option('--alt_name', type=str, default=None)
def main(config_files, name, alt_name):
    ''' Copy MS from the main directory, distributing them to different nodes using the CONFIG_FILE'''
    # Load configuration from TOML file
    for config_file in config_files:
        config = toml.load(config_file)
        base_dir = config['base_dir']

        nodes = [*worker.expend_num_ranges(config['nodes'])]
        nodes_cycle = itertools.cycle(nodes)
        channels = [*worker.expend_num_ranges(config['channels'])]

        if alt_name is None:
             alt_name = name

        freq = int(chanel_to_freq(channels[0]))
        ms_list_name = f'{alt_name}_f{freq}'

        with open(f'{out_dir}/{ms_list_name}', 'w') as f:
            for node, channel in zip(nodes_cycle, channels):
                f.write(f'/net/{node}/{base_dir}/{name}/ZW3_IFRQ_{channel}.ms\n')

if __name__ == '__main__':
    main()

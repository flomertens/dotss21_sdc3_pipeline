#!/bin/bash

out_dir=/data/users/lofareor/mertens/sdc3/ms_u70-1600/c450-599/data_sub_ateam_bright/
name=preflaf_u4
job_name=preflag_u45.joblist

rm -f ${job_name}
mkdir -p logs

dp3_param="msout=. steps=[uvwflag] uvwflag.uvlambdamin=45"

i=1

for c in $(seq -f "%04g" 450 599)
do
  n=$(printf "%02d" $i)
  msin="/net/node1${n}/${out_dir}/ZW3_IFRQ_${c}.ms"

#  cmd="DP3 msin=${msin} ${dp3_param}"
  flagtool reset ${msin}

#  echo $cmd

#  nodetool add_job ${job_name} "${cmd}" --log_file logs/ZW3_IFRQ_${c}.ms.filter_uvrange_ateam.log -t node1${n}

  i=$(( (i % 16) + 1 ))
done

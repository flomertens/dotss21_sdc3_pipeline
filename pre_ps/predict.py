#!/usr/bin/env python3
import os
import toml
import click
import time
import shutil
import itertools

from libpipe import worker
from traitlets import default


RUN_OSKAR = 'nice /net/node100/data/users/lofareor/mertens/sdc3/oskar/run_oskar.sh'


def chanel_to_freq(channel):
    return 106 + int(channel) / 10


def get_channels(channel_str):
    return sorted(set(channel for k in channel_str.split(',') for channel in worker.expend_num_ranges(k)))


@click.command()
@click.argument('config-file', type=click.Path(exists=True))
@click.argument('name', type=str)
@click.argument('sky_model', type=click.Path(exists=True))
@click.option('--max_concurrent', '-m', help='Maximum concurrent tasks on a node', type=int, default=5)
@click.option('--env_file', '-e', help='Environment source file', type=click.Path(exists=True))
@click.option('--dry_run', '-d', help='Dry run, only print commands', is_flag=True)
@click.option('--force', is_flag=True, help='Force execution without confirmation')
@click.option('--only_channels', help='Process only specific channels', default=None)
def main(config_file, name, sky_model, max_concurrent, env_file, dry_run, force, only_channels):
    ''' Predict visibilities using OSKAR'''
    print(only_channels)
    config = toml.load(config_file)
    base_dir = config['base_dir']
    oskar_template_setup_file = config['oskar_template_setup_file']
    input_time_steps = config['input_time_steps']
    avg_time_step = config['avg_time_step']
    uv_min = config['uv_min']
    uv_max = config['uv_max']

    if only_channels:
        only_channels = get_channels(only_channels)

    num_time_steps = input_time_steps // avg_time_step
    int_time = 10 * avg_time_step

    sky_model = os.path.abspath(sky_model)

    parameters = f"--num_chan 1 --num_time_steps {num_time_steps} --uv_min {uv_min} --uv_max {uv_max} --int_time {int_time}"

    nodes = [*worker.expend_num_ranges(config['nodes'])]
    nodes_cycle = itertools.cycle(nodes)
    channels = get_channels(config['channels'])

    if 'nodes_worker' in config:
        nodes_worker = [*worker.expend_num_ranges(config['nodes_worker'])]
    else:
        nodes_worker = nodes

    os.makedirs('logs', exist_ok=True)

    worker_pool = worker.WorkerPool(nodes_worker, name='Predict', max_tasks_per_worker=max_concurrent, 
                                    env_source_file=env_file, debug=dry_run, dry_run=dry_run)

    for node, channel in zip(nodes_cycle, channels):
        if only_channels and channel not in only_channels:
            continue

        # The path of the files for oskar should be local to the node (not via NFS)
        output_dir = f'/net/{node}/{base_dir}/{name}'
        msout = f"{output_dir}/ZW3_IFRQ_{channel}.ms"

        os.makedirs(output_dir, exist_ok=True)

        setup_full_ska = f"{output_dir}/ZW3_IFRQ_{channel}.ms.setup_full.ini"
        shutil.copy(oskar_template_setup_file, setup_full_ska)

        freq = chanel_to_freq(channel)

        cmd = f"{RUN_OSKAR} {setup_full_ska} {msout} {sky_model} {parameters} --start_freq {freq}"

        log_file = f'logs/ZW3_IFRQ_{channel}.ms.predict_{name}.log'
        print(cmd)
        worker_pool.add(cmd, output_file=log_file, run_on_host=node)

    confirm_execute = force or click.confirm('Do you want to execute the tasks?')

    if confirm_execute:
        time.sleep(1)
        worker_pool.execute()

if __name__ == '__main__':
    main()

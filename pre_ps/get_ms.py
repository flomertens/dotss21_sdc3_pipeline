#!/usr/bin/env python3
import os
import toml
import click
import itertools

from libpipe import worker

def chanel_to_freq(channel):
    return 106 + int(channel) / 10

@click.command()
@click.argument('config-file', type=click.Path(exists=True))
@click.argument('name', type=str)
def main(config_file, name):
    ''' Get the MS from the main directory'''
    # Load configuration from TOML file
    config = toml.load(config_file)
    base_dir = config['base_dir']

    nodes = [*worker.expend_num_ranges(config['nodes'])]
    nodes_cycle = itertools.cycle(nodes)
    channels = [*worker.expend_num_ranges(config['channels'])]

    freq = int(chanel_to_freq(channels[0]))
    ms_list_name = f'{name}_f{freq}'

    for node, channel in zip(nodes_cycle, channels):
        print(f'/net/{node}/{base_dir}/{name}/ZW3_IFRQ_{channel}.ms', end=' ')

if __name__ == '__main__':
    main()


#!/bin/bash

env_file=/home/users/mertens/.activate_pspipe_dev.sh

sky_model=/net/node100/data/users/lofareor/mertens/sdc3/sky_models/combined_v1.osm

channels='0[000-900]'

configs="config_c000-149.toml  config_c150-299.toml  config_c300-449.toml  config_c450-599.toml  config_c600-749.toml  config_c750-900.toml"

inp_data=data
model_data=combined_v1_model
out_data=data_sub_combined

for config in ${configs}
do
    ./copy_data.py ${config} data -m 1 --force --only_channels ${channels}
    ./predict.py ${config} ${model_data} ${sky_model} -m 1 --force --only_channels ${channels}
    ./filter_uvrange.py ${config} ${model_data} -m 1 --force --only_channels ${channels}
    ./sub_model.py ${config} ${inp_data} ${model_data} ${out_data} -m 1 -e ${env_file} --force --only_channels ${channels}
done

#!/usr/bin/env python3
import os
import toml
import click
import itertools

from libpipe import worker


def get_channels(channel_str):
    return sorted(set(channel for k in channel_str.split(',') for channel in worker.expend_num_ranges(k)))


@click.command()
@click.argument('config-file', type=click.Path(exists=True))
@click.argument('name', type=str)
@click.option('--dry_run', '-d', help='Dry run, only print commands', is_flag=True)
@click.option('--only_channels', help='Process only specific channels', default=None)
def main(config_file, name, dry_run, only_channels):
    ''' Copy MS from the main directory, distributing them to different nodes using the CONFIG_FILE'''
    # Load configuration from TOML file
    config = toml.load(config_file)
    base_dir = config['base_dir']

    if only_channels:
        only_channels = get_channels(only_channels)

    nodes = [*worker.expend_num_ranges(config['nodes'])]
    nodes_cycle = itertools.cycle(nodes)
    channels = worker.expend_num_ranges(config['channels'])

    os.makedirs('logs', exist_ok=True)

    worker_pool = worker.WorkerPool(nodes, name='Remove Data', max_tasks_per_worker=1,
                                    debug=dry_run, dry_run=dry_run)

    for node, channel in zip(nodes_cycle, channels):
        if only_channels and channel not in only_channels:
            continue

        output_dir = f'/net/{node}/{base_dir}/{name}'

        cmd = f'rm -rf {output_dir}/ZW3_IFRQ_{channel}.ms'

        log_file = f'logs/ZW3_IFRQ_{channel}.ms.remove.log'
        print(cmd)
        worker_pool.add(cmd, output_file=log_file, run_on_host=node)

    confirm_execute = click.confirm('Do you want to execute the tasks?')

    if confirm_execute:
        worker_pool.execute()

if __name__ == '__main__':
    main()

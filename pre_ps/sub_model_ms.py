#!/usr/bin/env python3

import os
import shutil
import argparse
import casacore.tables as tables

def create_ms_3(ms_1, ms_2, ms_3, chunk_size):
    if os.path.exists(ms_3):
        shutil.rmtree(ms_3)

    tables.tablecopy(ms_1, ms_3)

    # Read and subtract the data from ms_2 from ms_1 in chunks
    with tables.table(ms_1, readonly=True) as ms1_table, tables.table(ms_2, readonly=True) as ms2_table, tables.table(ms_3, readonly=False) as ms3_table:
        # Get the number of rows in the measurement sets
        num_rows = ms1_table.nrows()

        # Read and subtract the data from ms_2 from ms_1 in chunks
        start_row = 0
        while start_row < num_rows:
            # Calculate the end row for the current chunk
            end_row = int(min(start_row + chunk_size, num_rows))

            # Read the data chunk from ms_1 and ms_2
            ms1_data_chunk = ms1_table.getcol("DATA", startrow=start_row, nrow=int(end_row - start_row))
            ms2_data_chunk = ms2_table.getcol("DATA", startrow=start_row, nrow=int(end_row - start_row))

            # Subtract the data chunk from ms_2 from ms_1
            ms3_data_chunk = ms1_data_chunk - ms2_data_chunk

            # Write the data chunk to ms_3
            ms3_table.putcol("DATA", ms3_data_chunk, startrow=start_row, nrow=int(end_row - start_row))

            # Update the start row for the next chunk
            start_row = int(start_row + chunk_size)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Create ms_3 with data being the data of ms_1 minus the data of ms_2")
    parser.add_argument("ms_1", help="Path of the first measurement set")
    parser.add_argument("ms_2", help="Path of the second measurement set")
    parser.add_argument("ms_3", help="Path of the third measurement set")
    parser.add_argument("--chunk-size", type=int, default=2e7, help="Chunk size for reading and writing data (default: 2e7)")

    args = parser.parse_args()

    create_ms_3(args.ms_1, args.ms_2, args.ms_3, args.chunk_size)


#!/bin/bash

env_file=/home/users/mertens/.activate_pspipe_dev.sh

sky_model=/net/node100/data/users/lofareor/mertens/sdc3/sky_models/combined_v1.osm
sky_model=/net/node100/data/users/lofareor/mertens/sdc3/sky_models/diffuse_gaussian_and_point_components_trial7.osm

channels='0[000-900]'

configs="config_c300-449.toml"

inp_data=data_sub_combined
model_data=full_diffuse_t7
out_data=data_sub_combined_diffuse_t7

for config in ${configs}
do
    ./predict.py ${config} ${model_data} ${sky_model} -m 1 --force --only_channels ${channels}
    ./filter_uvrange.py ${config} ${model_data} -m 1 --force --only_channels ${channels}
    ./sub_model.py ${config} ${inp_data} ${model_data} ${out_data} -m 1 -e ${env_file} --force --only_channels ${channels}
done

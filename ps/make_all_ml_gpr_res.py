#!/usr/bin/env python3

from libpipe import worker

ex = '/net/node100/data/users/lofareor/mertens/sdc3/batch_run/ps/save_ml_gpr_ps_final.py'
env_file = '/home/users/mertens/.activate_pspipe_dev.sh'
nodes = [*worker.expend_num_ranges('node1[01-15]')]
out_dir = '/home/users/mertens/projects/challenges/sdc3/ml_gpr_results/run05/'
rev_name = '/net/node100/data/users/lofareor/mertens/sdc3/batch_run/ps/run05.toml'

datas = ['data_sub_combined', 'data_sub_combined_diffuse_t7']

worker_pool = worker.WorkerPool(nodes, name='Get GPR PS', max_tasks_per_worker=1, env_source_file=env_file)

for i, fmin in enumerate([106, 121, 136, 151, 166, 181]):
#for i, fmin in enumerate(range(106, 166, 10)):
    for name in datas:
        for ml_gpr_name in ['ml_gpr_t4']:
            cmd = f'{ex} {rev_name} {name}_f{fmin} {ml_gpr_name} {out_dir}'
            worker_pool.add(cmd)

worker_pool.execute()


run_ml_gpr() {
    local gpr_name=$1
    local rev=$2

    pspipe run_ml_gpr ${rev}.toml data_sub_combined_diffuse_t7_f106,data_sub_combined_f106 worker.nodes="'node101,node102'" ml_gpr.config="'config/gpr_config_ml_10_${gpr_name}.toml'" ml_gpr.name="'ml_gpr_${gpr_name}'" &
    pspipe run_ml_gpr ${rev}.toml data_sub_combined_diffuse_t7_f121,data_sub_combined_f121 worker.nodes="'node103,node104'" ml_gpr.config="'config/gpr_config_ml_10_${gpr_name}.toml'" ml_gpr.name="'ml_gpr_${gpr_name}'" &
    pspipe run_ml_gpr ${rev}.toml data_sub_combined_diffuse_t7_f136,data_sub_combined_f136 worker.nodes="'node105,node106'" ml_gpr.config="'config/gpr_config_ml_9_${gpr_name}.toml'" ml_gpr.name="'ml_gpr_${gpr_name}'" &
    pspipe run_ml_gpr ${rev}.toml data_sub_combined_diffuse_t7_f151,data_sub_combined_f151 worker.nodes="'node107,node108'" ml_gpr.config="'config/gpr_config_ml_9_${gpr_name}.toml'" ml_gpr.name="'ml_gpr_${gpr_name}'" &
    pspipe run_ml_gpr ${rev}.toml data_sub_combined_diffuse_t7_f166,data_sub_combined_f166 worker.nodes="'node109,node110'" ml_gpr.config="'config/gpr_config_ml_8_${gpr_name}.toml'" ml_gpr.name="'ml_gpr_${gpr_name}'" &
    pspipe run_ml_gpr ${rev}.toml data_sub_combined_diffuse_t7_f181,data_sub_combined_f181 worker.nodes="'node111,node112'" ml_gpr.config="'config/gpr_config_ml_8_${gpr_name}.toml'" ml_gpr.name="'ml_gpr_${gpr_name}'" 

    pspipe run_ml_gpr ${rev}.toml data_sub_combined_f106 worker.nodes="'node101,node102'" ml_gpr.config="'config/gpr_config_ml_10_${gpr_name}.toml'" ml_gpr.name="'ml_gpr_${gpr_name}'" &
    pspipe run_ml_gpr ${rev}.toml data_sub_combined_f121 worker.nodes="'node103,node104'" ml_gpr.config="'config/gpr_config_ml_10_${gpr_name}.toml'" ml_gpr.name="'ml_gpr_${gpr_name}'" &
    pspipe run_ml_gpr ${rev}.toml data_sub_combined_f136 worker.nodes="'node105,node106'" ml_gpr.config="'config/gpr_config_ml_9_${gpr_name}.toml'" ml_gpr.name="'ml_gpr_${gpr_name}'" &
    pspipe run_ml_gpr ${rev}.toml data_sub_combined_f151 worker.nodes="'node107,node108'" ml_gpr.config="'config/gpr_config_ml_9_${gpr_name}.toml'" ml_gpr.name="'ml_gpr_${gpr_name}'" &
    pspipe run_ml_gpr ${rev}.toml data_sub_combined_f166 worker.nodes="'node109,node110'" ml_gpr.config="'config/gpr_config_ml_8_${gpr_name}.toml'" ml_gpr.name="'ml_gpr_${gpr_name}'" &
    pspipe run_ml_gpr ${rev}.toml data_sub_combined_f181 worker.nodes="'node111,node112'" ml_gpr.config="'config/gpr_config_ml_8_${gpr_name}.toml'" ml_gpr.name="'ml_gpr_${gpr_name}'" &
}

run_ml_gpr t4 run05

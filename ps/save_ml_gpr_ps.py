#!/usr/bin/env python3

import os
import click
import numpy as np
from pspipe import database, settings

@click.command()
@click.argument('rev_file_name', type=click.Path(exists=True))
@click.argument('obs_ids', type=str)
@click.argument('ml_gpr_name', type=str)
@click.argument('out_dir', type=click.Path())
def main(rev_file_name, obs_ids, ml_gpr_name, out_dir):
    rev = database.VisRevision(settings.Settings.load_with_defaults(rev_file_name))

    for obs_id in obs_ids.split(','):
        data_r1 = rev.get_data(obs_id)
        ps_gen_1 = data_r1.get_ps_gen(ft_method='nudft', window_fct='hann', primary_beam='ska_low', rmean_freqs=False, du=8, umin=30, umax=550)
        ps_gen_fg_1 = data_r1.get_ps_gen(ft_method='nudft', window_fct='blackmanharris', primary_beam='ska_low', rmean_freqs=False, du=8, umin=30, umax=550)

        fmin = int(data_r1.i.freqs[0] * 1e-6)
        i = int((fmin - 106) / 15)

        ml_res_1 = data_r1.get_ml_gpr_res(i, 30, 550, name=ml_gpr_name)

        kbins = np.logspace(np.log10(0.05), np.log10(1.2), 10)

        ps3d_eor_1 = ml_res_1.get_ps_eor(ps_gen_1, kbins, n_pick=20)
        ps3d_res_1 = ml_res_1.get_ps_res(ps_gen_1, kbins, n_pick=20)
        ps3d_fg_1 = ml_res_1.get_ps_fg(ps_gen_fg_1, kbins, n_pick=20)

        os.makedirs(f'{out_dir}/{ml_gpr_name}/{obs_id}', exist_ok=True)

        ps3d_eor_1.save(f'{out_dir}/{ml_gpr_name}/{obs_id}', 'ps_eor_h')
        ps3d_res_1.save(f'{out_dir}/{ml_gpr_name}/{obs_id}', 'ps_res_h')
        ps3d_fg_1.save(f'{out_dir}/{ml_gpr_name}/{obs_id}', 'ps_fg')
        print('Done')

if __name__ == '__main__':
    main()

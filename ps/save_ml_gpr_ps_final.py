#!/usr/bin/env python3
 
import os
import click
import numpy as np
from pspipe import database, settings
from ps_eor import psutil, pspec

from astropy.cosmology import FlatLambdaCDM
psutil.set_cosmology(FlatLambdaCDM(100, Om0=0.30964))

def add_noise_err_2d(ps2d, ps2d_noise):
    errs = np.random.normal(loc=0, scale=ps2d_noise.err[:, :, None], size=(len(ps2d.k_par), len(ps2d.k_per), len(ps2d.all_ps)))
    errs_cv = np.random.normal(loc=0, scale=ps2d.data[:, :, None] * np.sqrt(2 / ps2d_noise.n_eff[:, :, None]), size=(len(ps2d.k_par), len(ps2d.k_per), len(ps2d.all_ps)))
    all_ps = [ps.new_with_data(ps.data + err + err_cv, 0) for ps, err, err_cv in zip(ps2d.all_ps, errs.transpose(2, 0, 1), errs_cv.transpose(2, 0, 1))]
    return ps2d.new_with_data(ps2d.data, pspec.CylindricalPowerSpectraMC(all_ps).err)


@click.command()
@click.argument('rev_file_name', type=click.Path(exists=True))
@click.argument('obs_ids', type=str)
@click.argument('ml_gpr_name', type=str)
@click.argument('out_dir', type=click.Path())
def main(rev_file_name, obs_ids, ml_gpr_name, out_dir):
    rev = database.VisRevision(settings.Settings.load_with_defaults(rev_file_name))

    for obs_id in obs_ids.split(','):
        data_r1 = rev.get_data(obs_id)

        fmin = int(data_r1.i.freqs[0] * 1e-6)
        i = int((fmin - 106) / 15)

        ml_res = data_r1.get_ml_gpr_res(i, 30, 550, name=ml_gpr_name)
        noise_cube = ml_res.get_noise_cube()
        noise_cube.weights = ml_res.get_data_cube().weights.copy()

        z = psutil.freq_to_z(noise_cube.freqs.mean())
        du = psutil.k_to_l(0.05, z) / (2 * np.pi)
        umin = du / 2.
        umax = psutil.k_to_l(0.5, z) / (2 * np.pi) + du / 2
        M = int((1 / psutil.k_to_delay(0.05, z)) / 0.1e6)
        ps_gen_bh = data_r1.get_ps_gen(ft_method='nudft', window_fct='hann', primary_beam='ska_low', rmean_freqs=False, du=du, umin=umin, umax=umax, uniform_u_bins=True)
        ps_gen_bh.eor.M = M
        ps_gen_bh._compute_delays()
#        print(ps_gen_bh.k_par)
#        print(ps_gen_bh.k_per)

        kbins = np.logspace(np.log10(0.05), np.log10(1.2), 10)

        ps2d_res = ml_res.get_ps_res(ps_gen_bh, kbins, n_pick=20).get_ps2d()
        ps2d_final = (ps2d_res - ps_gen_bh.get_ps2d(noise_cube))

        ps2d_eor = ml_res.get_ps_eor(ps_gen_bh, kbins, n_pick=50).get_ps2d()
        ps2d_noise = ps_gen_bh.get_ps2d(noise_cube)
        ps2d_eor = add_noise_err_2d(ps2d_eor, ps2d_noise)

        os.makedirs(f'{out_dir}/{ml_gpr_name}/{obs_id}', exist_ok=True)

        ps2d_eor.save_to_txt(f'{out_dir}/{ml_gpr_name}/{obs_id}/ps2d_final_eor_cv.h5')
        ps2d_final.save(f'{out_dir}/{ml_gpr_name}/{obs_id}/ps2d_final_cv.h5')
        print('Done')

if __name__ == '__main__':
    main()

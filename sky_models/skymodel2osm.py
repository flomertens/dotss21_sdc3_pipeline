import csv

from lsmtool import tableio, skymodel

input_model = '/net/node100/data/users/lofareor/mertens/sdc3/test_sub_bright/bright.skymodel'
output_model = '/net/node100/data/users/lofareor/mertens/sdc3/oskar/bright.osm'

sm_pb = tableio.skyModelReader(input_model)
coords_sm_pb = SkyCoord(ra=sm_pb['Ra'].data, dec=sm_pb['Dec'].data, unit=u.deg)
data = [coords_sm_pb.ra.deg, coords_sm_pb.dec.deg, 
                sm_pb['I'], [0] * len(sm_pb), [0] * len(sm_pb), [0] * len(sm_pb), sm_pb['ReferenceFrequency'], sm_pb['SpectralIndex'].value[:, 0],
                        [0] * len(sm_pb), sm_pb['MajorAxis'], sm_pb['MinorAxis'], sm_pb['Orientation']]

with open(output_model, 'w') as file:
    for sublist in np.array(data).T:
        line = ' '.join(str(element) for element in sublist)
        file.write(line + '\n')

